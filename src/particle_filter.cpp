/*
 * particle_filter.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: Tiffany Huang
 */

#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>

#include "particle_filter.h"

using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
	// TODO: Set the number of particles. Initialize all particles to first position (based on estimates of
	//   x, y, theta and their uncertainties from GPS) and all weights to 1.
	// Add random Gaussian noise to each particle.
	// NOTE: Consult particle_filter.h for more information about this method (and others in this file).

	num_particles = 200;
	weights.resize(num_particles, 1.0f);
	double std_x = std[0];
	double std_y = std[1];
	double std_theta = std[2];

	// Create a normal (Gaussian) distribution for x, y and theta.
	normal_distribution<double> dist_x(x, std_x);
	normal_distribution<double> dist_y(y, std_y);
	normal_distribution<double> dist_theta(theta, std_theta);

	for(int i = 0; i < num_particles; ++i){
		Particle p;
		p.id = i;

		p.x = dist_x(gen);
		p.y = dist_y(gen);
		p.theta = dist_theta(gen);
		p.weight = 1.0;

		particles.push_back(p);
	}

	//printWeights(particles);

	is_initialized = true;
}

void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate) {
	// TODO: Add measurements to each particle and add random Gaussian noise.
	// NOTE: When adding noise you may find std::normal_distribution and std::default_random_engine useful.
	//  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
	//  http://www.cplusplus.com/reference/random/default_random_engine/

	// This is called after initialization is done with the very first measurement
	// for subsequent measurements we just predict using corresponding formulae

	double std_x = std_pos[0];
	double std_y = std_pos[1];
	double std_theta = std_pos[2];

	for(int i = 0; i< num_particles; ++i) {
		double prev_x = particles[i].x;
		double prev_y = particles[i].y;
		double prev_theta = particles[i].theta;
		double yaw_increment = yaw_rate * delta_t;

		double x_f;
		double y_f;
		double theta_f;

		if(std::fabs(yaw_rate) <= 0.01){
			x_f = prev_x + velocity * delta_t * cos(prev_theta);
			y_f = prev_y + velocity * delta_t * sin(prev_theta);
			theta_f = prev_theta;
		}else{
			x_f = prev_x + (velocity/yaw_rate)*(sin(prev_theta + yaw_increment) - sin(prev_theta));
			y_f = prev_y + (velocity/yaw_rate)*(cos(prev_theta) - cos(prev_theta + yaw_increment));
			theta_f = prev_theta + yaw_increment;
		}

		// Create a normal (Gaussian) distribution for x_f, y_f and theta_f.
		normal_distribution<double> dist_x_f(x_f, std_x);
		normal_distribution<double> dist_y_f(y_f, std_y);
		normal_distribution<double> dist_theta_f(theta_f, std_theta);

		//update the initial particle position and heading
		particles[i].x = dist_x_f(gen);
		particles[i].y = dist_y_f(gen);
		particles[i].theta = dist_theta_f(gen);
	}

}

void ParticleFilter::dataAssociation(std::vector<LandmarkObs> predicted, std::vector<LandmarkObs>& observations) {
	// TODO: Find the predicted measurement that is closest to each observed measurement and assign the
	//   observed measurement to this particular landmark.
	// NOTE: this method will NOT be called by the grading code. But you will probably find it useful to
	//   implement this method and use it as a helper during the updateWeights phase.

	double min_distance, dist, xdiff, ydiff;
  int landmark_id;

  for(int index = 0; index < observations.size(); index++)
  {
    LandmarkObs obs = observations[index];

    min_distance = INFINITY;
    landmark_id = -1;
    for(unsigned i = 0; i < predicted.size(); i++)
    {
      auto pred_lm = predicted[i];
      xdiff = (pred_lm.x - obs.x);
      ydiff = (pred_lm.y - obs.y);
      dist = xdiff*xdiff + ydiff*ydiff;
      if(dist < min_distance)
      {
        min_distance = dist;
        landmark_id = i;
      }
    }
		// Associate the landmark id with this observation
    observations[index].id = landmark_id;
  }
}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[],
		std::vector<LandmarkObs> observations, Map map_landmarks) {
	// TODO: Update the weights of each particle using a mult-variate Gaussian distribution. You can read
	//   more about this distribution here: https://en.wikipedia.org/wiki/Multivariate_normal_distribution
	// NOTE: The observations are given in the VEHICLE'S coordinate system. Your particles are located
	//   according to the MAP'S coordinate system. You will need to transform between the two systems.
	//   Keep in mind that this transformation requires both rotation AND translation (but no scaling).
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html

	for(int i = 0; i< num_particles; ++i){

		double weight = 1.0f; // multivariate guassian probability

		Particle p = particles[i];
		//step1: For each particle, get a list of landmarks in sensor range
		std::vector<LandmarkObs> probable_landmarks;
		getInRangeLandmarks(probable_landmarks, p, map_landmarks, sensor_range);

		//step2: convert the observations to map coordinate system
		// transform to map x coordinate
		std::vector<LandmarkObs> observations_transformed;
		vehicleToMapCoordinates(observations_transformed, observations, p);

		//step3: Data association - We need to associate the landmark id with each observation for this particle
		// we do this by finding the landmarks in the map that are nearest to the observations
		dataAssociation(probable_landmarks, observations_transformed);

		//step4: All observations have landmark ids now
		// iterate over all landmarks for this particle and calculate multivariate gaussian probability

		for(int j = 0; j < observations_transformed.size(); ++j){
			LandmarkObs noisy_obs =  observations_transformed[j];
			LandmarkObs ground_truth_obs = probable_landmarks[noisy_obs.id];

			//cout << "Observation:" << endl;
			//cout << "id:" << noisy_obs.id << " x = " << noisy_obs.x << " y = " << noisy_obs.y << endl;
			//cout << "Ground truth:" << endl;
			//cout << "x = " << ground_truth_obs.x << " y = " << ground_truth_obs.y << endl << endl;

			double multi_gaus_prob = calculateProbability(noisy_obs, ground_truth_obs, std_landmark);
			weight *= multi_gaus_prob;
		}
		particles[i].weight = weight;
		weights[i] = weight;
	}
	//printWeights(particles);
}

void ParticleFilter::resample() {
	// TODO: Resample particles with replacement with probability proportional to their weight.
	// NOTE: You may find std::discrete_distribution helpful here.
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
	std::discrete_distribution<int> distribution(weights.begin(), weights.end());
  std::vector<Particle> resampled_particles;

  for(int i = 0; i < num_particles; ++i)
  {
    int ind = distribution(gen);
    resampled_particles.push_back(particles[ind]);
  }
  particles = resampled_particles;

}

Particle ParticleFilter::SetAssociations(Particle particle, std::vector<int> associations, std::vector<double> sense_x, std::vector<double> sense_y)
{
	//particle: the particle to assign each listed association, and association's (x,y) world coordinates mapping to
	// associations: The landmark id that goes along with each listed association
	// sense_x: the associations x mapping already converted to world coordinates
	// sense_y: the associations y mapping already converted to world coordinates

	//Clear the previous associations
	particle.associations.clear();
	particle.sense_x.clear();
	particle.sense_y.clear();

	particle.associations= associations;
 	particle.sense_x = sense_x;
 	particle.sense_y = sense_y;

 	return particle;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<int>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}


void ParticleFilter::getInRangeLandmarks(std::vector<LandmarkObs>& predicted_landmarks, Particle p, Map map_landmarks, double sensor_range){
	for(int i = 0; i < map_landmarks.landmark_list.size(); ++i){
		LandmarkObs landmark;
		landmark.id = map_landmarks.landmark_list[i].id_i;
		landmark.x = map_landmarks.landmark_list[i].x_f;
		landmark.y = map_landmarks.landmark_list[i].y_f;
		double distance = dist(landmark.x, landmark.y, p.x, p.y);

    // In landmark is in range, add to the list
    if(distance <= sensor_range){predicted_landmarks.push_back(landmark);}

	}
}

	void ParticleFilter::vehicleToMapCoordinates(std::vector<LandmarkObs>& observations_transformed, std::vector<LandmarkObs> observations, Particle p){

		double x_part = p.x;
		double y_part = p.y;
		double theta = p.theta;

		for(int j = 0; j < observations.size(); ++j){
			double x_obs = observations[j].x;
			double y_obs = observations[j].y;

			//transform vehicle to map coordinates
			double x_map= x_part + (cos(theta) * x_obs) - (sin(theta) * y_obs);
			double y_map= y_part + (sin(theta) * x_obs) + (cos(theta) * y_obs);

			LandmarkObs observation;
			observation.x = x_map;
			observation.y = y_map;
			observations_transformed.push_back(observation);
		}
}

double ParticleFilter::calculateProbability(LandmarkObs noisy_obs, LandmarkObs ground_truth_obs, double std_landmark[]){

	double sig_x = std_landmark[0];
	double sig_x_sqr= sig_x * sig_x;

	double sig_y = std_landmark[1];
	double sig_y_sqr= sig_y * sig_y;

	double x_obs= noisy_obs.x;
	double y_obs= noisy_obs.y;
	double mu_x= ground_truth_obs.x;
	double mu_y= ground_truth_obs.y;

	double x_diff = x_obs - mu_x;
	double y_diff = y_obs - mu_y;


	double gauss_norm= (1/(2 * M_PI * sig_x * sig_y));
	double exponent= (x_diff*x_diff)/(2 * sig_x_sqr) + (y_diff*y_diff)/(2 * sig_y_sqr);
	double weight= gauss_norm * exp(-exponent);
	//cout << "Calculated weight: " << weight << endl;
	return weight;

}

void ParticleFilter::printWeights(std::vector<Particle> particles){
	//cout << "Particle weights for this iteration: " << endl;
	for(Particle p : particles){
		cout << p.weight << ", ";
	}
	cout << endl;
}
