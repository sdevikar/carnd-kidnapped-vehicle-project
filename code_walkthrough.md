# How the code works:

## Initialization
- First things that we did during the initialization phase, is that we created
n number of particles.
- All these particles are initialized to weight 1 to begin with
- Their initial position is somewhere around the first measurement received from
the GPS. This is denoted by sense_x, sense_y and sense_theta
- The position is "somewhere around", because we added the random noise with given
standard deviation for each particle
- The end result is a vector of particle objects, with their id, xy positions, theta and weight initialized
- sense_x, sense_y and associations-vector is not initialized yet

## Prediction
- For this step, we only use the previous velocity and previous yaw rate
- We take these, add it to the current position of each particle and predict their
new position
- For predicting the position, we use simple bicycle model CTRV formulae
- At the end of it, we'll have updated positions for each particle

## Weight update
- Here, we use observations and map as an input, along with the standard deviation of noise in observation measurements
- Observations is a list of LandmarkObs objects. LandmarkObs contain
  ```
  struct LandmarkObs {

  	int id;				// Id of matching landmark in the map.
  	double x;			// Local (vehicle coordinates) x position of landmark observation [m]
  	double y;			// Local (vehicle coordinates) y position of landmark observation [m]
  };
  ```
- Map has a list of landmark objects. Each landmark has an ID, and x, y position as we can see here

```
struct single_landmark_s{

  int id_i ; // Landmark ID
  float x_f; // Landmark x-position in the map (global coordinates)
  float y_f; // Landmark y-position in the map (global coordinates)
};
```

- For multivariate gaussian probability density function, we need
  - x_i: ith landmark measurement for one particular particle (i.e. x and y observed position)
  - μi: Predicted measurement for map landmark
- In other words, x and y are the observations in map coordinates from the landmarks quiz. (In landmarks quiz we took the observations in vehicle coordinates and converted them to map coordinates). These observations were the observations of distances from the landmarks
- μx, μy are the coordinates of the "nearest landmarks".
- We calculate this for each observation for a particle and multiply all of the probabilities
